﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {

	public float Speed = 1;
	public float RotateSpeed = 1;

	// Use this for initialization
	void Start () {
		if (Random.Range (0, 2) == 0) {
			clockwise = -1;
		}

		Speed = Speed + (Speed / (float)GameManager.Instance.ModifierTime);
	}

	void LateStart() {
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Instance.GameOver) {
			return;
		}
		if (GameManager.Instance.GamePause) {
			return;
		}
			
		if (transform.position.y < -12) {
			GameManager.Instance.AsteroidsPool.ReleaseObject (gameObject);
		}
	}

	void FixedUpdate() {
		if (GameManager.Instance.GameOver) {
			return;
		}
		if (GameManager.Instance.GamePause) {
			return;
		}
		transform.Rotate (0, 0, Time.fixedDeltaTime * clockwise * RotateSpeed);

		transform.position += Vector3.down * Speed * Time.fixedDeltaTime;
	}

	int clockwise = 1;
}
