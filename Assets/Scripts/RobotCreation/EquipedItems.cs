﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class EquipedItems : MonoBehaviour {

	public Equipment HeadRapide;
	public Equipment HeadDefense;
	public Equipment HeadMix;

	public Equipment TorsoRapide;
	public Equipment TorsoDefense;
	public Equipment TorsoMix;

	public Equipment ArmsRapide;
	public Equipment ArmsDefense;
	public Equipment ArmsMix;

	[HideInInspector]
	public Equipment equipedHeadE;
	[HideInInspector]
	public Equipment equipedTorsoE;
	[HideInInspector]
	public Equipment equipedArmsE;

	public List<GameObject> models;

	int equipedHead = 0;
	int equipedTorso = 0;
	int equipArms = 0;

	void SaveGO(Equipment e){
		var seri = new XmlSerializer (typeof(Equipment));
		var stream = new FileStream (Application.persistentDataPath + "/" + e.Name, FileMode.Create);
		seri.Serialize (stream, e);
		stream.Close ();
	}

	int id = 0;

	void Load(ref Equipment e, string path) {
		var seri = new XmlSerializer (typeof(Equipment));
		var stream = new FileStream (Application.persistentDataPath + "/" + path, FileMode.Open);
		e = seri.Deserialize (stream) as Equipment;
		for (int i = 0; i < e.Equipments.Count; ++i) {
			e.Equipments [i].Model = GameObject.Instantiate (models [id]);
			e.Equipments [i].Model.SetActive (false);
			e.Equipments [i].Model.transform.parent = transform;
			e.Equipments [i].Model.transform.localPosition = Vector3.zero;
			++id;

			if (e.Equiped == 1) {
				if (e.Equipments [0].GearType == Equipment.gearType.Head) {
					equipedHeadE = e;
				} else if (e.Equipments [0].GearType == Equipment.gearType.Body) {
					equipedTorsoE = e;
				} else if (e.Equipments [0].GearType == Equipment.gearType.Arms) {
					equipedArmsE = e;
				}
			}
		}
		stream.Close ();
	}

	void InitEquip(ref Equipment e) {
	}

	void InitEquips() {
		/*InitEquip (ref HeadRapide);
		InitEquip (ref HeadDefense);
		InitEquip (ref HeadMix);

		InitEquip (ref TorsoRapide);
		InitEquip (ref TorsoDefense);
		InitEquip (ref TorsoMix);
	
		InitEquip (ref ArmsRapide);
		InitEquip (ref ArmsDefense);
		InitEquip (ref ArmsMix);*/

		equipedHeadE.Equipments [equipedHeadE.EquipedUpgrade].Model.SetActive (true);
		equipedTorsoE.Equipments [equipedTorsoE.EquipedUpgrade].Model.SetActive (true);
		equipedArmsE.Equipments [equipedArmsE.EquipedUpgrade].Model.SetActive (true);
	}

	public void SaveStates() {
		SaveGO (HeadRapide);
		SaveGO (HeadDefense);
		SaveGO (HeadMix);

		SaveGO (TorsoRapide);
		SaveGO (TorsoDefense);
		SaveGO (TorsoMix);
	
		SaveGO (ArmsRapide);
		SaveGO (ArmsDefense);
		SaveGO (ArmsMix);
	}

	public void LoadStates() {
		Load (ref HeadRapide, "HeadRapide");
		Load (ref HeadDefense, "HeadDefense");
		Load (ref HeadMix, "HeadMix");

		Load (ref TorsoRapide, "TorsoRapide");
		Load (ref TorsoDefense, "TorsoDefense");
		Load (ref TorsoMix, "TorsoMix");

		Load (ref ArmsRapide, "ArmsRapide");
		Load (ref ArmsDefense, "ArmsDefense");
		Load (ref ArmsMix, "ArmsMix");
	}

	// Use this for initialization
	void Awake () {

		if (PlayerPrefs.GetInt("FirstTime", 1) == 1) {
			SaveStates();
			PlayerPrefs.SetInt("FirstTime", 0);
		}
		//SaveStates ();

		LoadStates();
		InitEquips ();


		equipedHead = PlayerPrefs.GetInt ("EquipedHead", 0);
		equipedTorso = PlayerPrefs.GetInt ("EquipedTorso", 0);
		equipArms = PlayerPrefs.GetInt ("EquipedArms", 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
