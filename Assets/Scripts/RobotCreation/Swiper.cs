﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Swiper : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public GameObject Robot;

	public void OnPointerDown(PointerEventData eventData) {
		if (eventData.button == 0) {
			m_oldPosition = eventData.position;
			clicked = true;
		}
	}

	public void OnPointerUp(PointerEventData eventData) {
		clicked = false;
	}

	void Update(){
		if (clicked) {
			m_dist = ((m_oldPosition - Input.mousePosition).x) / 5;
			Robot.transform.Rotate (0, m_dist, 0);

			m_oldPosition = Input.mousePosition;
		}
	}

	Vector3 m_oldPosition = Vector3.zero;
	float m_dist = 0;

	bool clicked = false;
}
