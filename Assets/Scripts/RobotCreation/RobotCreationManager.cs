﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RobotCreationManager : MonoBehaviour {

	public GameObject Robot;

	private int levelAchieved = 0;

	public List<GameObject> Panels;

	// Use this for initialization
	void Awake () {

		m_coins = PlayerPrefs.GetInt ("Coins", 0);
		levelAchieved = PlayerPrefs.GetInt ("LevelAchieved", 1);
	}

	void SetupPanel(GameObject panel, Equipment e) {
		if (e.Bought == 1) {
			if (e.Equiped == 0) {
				panel.transform.Find ("GenericButton").gameObject.SetActive (true);
				panel.transform.Find ("GenericButton").GetComponentInChildren<Text> ().text = "Equip";
				panel.transform.Find ("GenericButton").GetComponent<Button> ().onClick.RemoveAllListeners ();
				panel.transform.Find ("GenericButton").GetComponent<Button> ().onClick.AddListener(() => {
					Equip(e);
				});

				if (e.BoughtUpgrade < e.Equipments.Count) {
					if (e.Equipments [e.BoughtUpgrade + 1].Price > PlayerPrefs.GetInt ("Coins", 0)) {
						panel.transform.Find ("GenericButton").gameObject.SetActive (false);
					} else {
						panel.transform.Find ("GenericButton2").gameObject.SetActive (true);
						panel.transform.Find ("GenericButton2").GetComponentInChildren<Text> ().text = "Upgrade";
						panel.transform.Find ("GenericButton2").GetComponent<Button> ().onClick.RemoveAllListeners ();
						panel.transform.Find ("GenericButton2").GetComponent<Button> ().onClick.AddListener (() => {
							Upgrade (e);
						});
					}
				} else {
					panel.transform.Find ("GenericButton2").gameObject.SetActive (false);
				}
			} else {
				if (e.BoughtUpgrade < e.Equipments.Count) {
					if (e.Equipments [e.BoughtUpgrade + 1].Price > PlayerPrefs.GetInt ("Coins", 0)) {
						panel.transform.Find ("GenericButton").gameObject.SetActive (false);
					} else {
						panel.transform.Find ("GenericButton").gameObject.SetActive (true);
						panel.transform.Find ("GenericButton").GetComponentInChildren<Text> ().text = "Upgrade";
						panel.transform.Find ("GenericButton").GetComponent<Button> ().onClick.RemoveAllListeners ();
						panel.transform.Find ("GenericButton").GetComponent<Button> ().onClick.AddListener (() => {
							Upgrade (e);
						});
					}
				} else {
					panel.transform.Find ("GenericButton").gameObject.SetActive (false);
				}
				panel.transform.Find ("GenericButton2").gameObject.SetActive(false);
			}

		} else {
			if (e.Equipments [0].Price > PlayerPrefs.GetInt ("Coins", 0)) {
				panel.transform.Find ("GenericButton").gameObject.SetActive (false);
			} else {
				panel.transform.Find ("GenericButton").gameObject.SetActive (true);
				panel.transform.Find ("GenericButton").GetComponentInChildren<Text> ().text = "Buy";
				panel.transform.Find ("GenericButton").GetComponent<Button> ().onClick.RemoveAllListeners ();
				panel.transform.Find ("GenericButton").GetComponent<Button> ().onClick.AddListener (() => {
					Buy (e);
				});
			}
			panel.transform.Find ("GenericButton2").gameObject.SetActive (false);
		}

		panel.transform.Find ("Name").GetComponent<TMPro.TextMeshProUGUI> ().text = e.Name;
		panel.transform.Find ("Name").GetComponent<TMPro.TextMeshProUGUI> ().color = e.Equiped == 1 ? Color.blue : Color.white;
		panel.transform.Find ("PriceText").GetComponent<Text> ().text = "" + e.Equipments [e.BoughtUpgrade].Price;
		panel.transform.Find ("AgilityText").GetComponent<Text>().text = "" + e.GetAgility;
		panel.transform.Find ("ForceText").GetComponent<Text>().text = "" + e.GetLife;
		panel.transform.Find ("SpeedText").GetComponent<Text>().text = "" + e.GetSpeedToFinish;
	}

	void Start() {
		Init ();
	}

	string[] levels = {
		"0",
		"1",
		"2",
	};

	public void LoadLevel() {
		int nextLevel = PlayerPrefs.GetInt ("CurrentLevel", 0);
		SceneManager.LoadScene (levels[nextLevel]);
	}

	public void Init(){
		EquipedItems e = GameObject.Find("Robot").GetComponent<EquipedItems> ();

		SetupPanel (Panels [0], e.HeadRapide);
		SetupPanel (Panels [1], e.HeadDefense);
		SetupPanel (Panels [2], e.HeadMix);

		SetupPanel (Panels [3], e.TorsoRapide);
		SetupPanel (Panels [4], e.TorsoDefense);
		SetupPanel (Panels [5], e.TorsoMix);

		SetupPanel (Panels [6], e.ArmsRapide);
		SetupPanel (Panels [7], e.ArmsDefense);
		SetupPanel (Panels [8], e.ArmsMix);

		GameObject.Find ("GoldPanel").GetComponentInChildren<Text> ().text = "" + PlayerPrefs.GetInt ("Coins", 0);
	}

	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate() {

	
	
	}

	public void Upgrade(Equipment e) {

		++e.BoughtUpgrade;
		e.EquipedUpgrade = e.BoughtUpgrade;
		Init ();
		Robot.GetComponent<EquipedItems> ().SaveStates ();
		PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins", 0) - e.Equipments [e.BoughtUpgrade].Price);
		UpdateCoins ();
		Debug.Log ("Upgrading");
	}

	public void Buy(Equipment e) {
		e.Bought = 1;
		Init ();
		Robot.GetComponent<EquipedItems> ().SaveStates ();
		PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins", 0) - e.Equipments [0].Price);
		UpdateCoins ();
		Debug.Log ("Upgrading");
	}

	public void UpdateCoins(){
	}

	public void Equip(Equipment e) {
		e.Equiped = 1;
		if (e.Equipments[0].GearType == Equipment.gearType.Arms) {
			Robot.GetComponent<EquipedItems> ().equipedArmsE.Equiped = 0;
			Robot.GetComponent<EquipedItems> ().equipedArmsE = e;
		}
		else if (e.Equipments[0].GearType == Equipment.gearType.Body) {
			Robot.GetComponent<EquipedItems> ().equipedTorsoE.Equiped = 0;
			Robot.GetComponent<EquipedItems> ().equipedTorsoE = e;
		}
		else if (e.Equipments[0].GearType == Equipment.gearType.Head) {
			Robot.GetComponent<EquipedItems> ().equipedHeadE.Equiped = 0;
			Robot.GetComponent<EquipedItems> ().equipedHeadE = e;
		}

		Init ();
		Robot.GetComponent<EquipedItems> ().SaveStates ();
	}



	Vector3 m_oldPosition = Vector3.zero;
	float m_dist = 0;
	int m_coins;

}
