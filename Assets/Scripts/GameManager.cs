﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

	#region SINGLETON PATTERN
	public static GameManager _instance;
	public static GameManager Instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<GameManager>();

				if (_instance == null)
				{
					GameObject container = new GameObject("GameManager");
					_instance = container.AddComponent<GameManager>();
				}
			}

			return _instance;
		}
	}
	#endregion

	[Header("Asteroids Values")]
	public AnimationCurve AsteroidsFrequencyCurve;
	public AnimationCurve AsteroidsSpeedMin;
	public AnimationCurve AsteroidsSpeedMax;


	[Space(30)]
	[Header("Bonuses Values")]
	public AnimationCurve BonusFrequencyCurve;
	public AnimationCurve BonusSpeedMin;
	public AnimationCurve BonusSpeedMax;

	[Space(30)]
	[Header("Misc")]
	public float TimeOfGame = 60;
	[HideInInspector]
	public float ModifiedTimeOfGame;
	[HideInInspector]
	public float ModifierTime;
	[HideInInspector]
	public float TimeAfterComputing = 0;

	Slider slider;

	[HideInInspector]
	public int Coins;

	void Start() {
		AsteroidsPool = GameObject.Find ("AsteroidsPool").GetComponent<Pool>();
		BonusesPool = GameObject.Find ("BonusesPool").GetComponent<Pool> ();

		asteroidToWait = AsteroidsFrequencyCurve.Evaluate (timer);
		bonusToWait = BonusFrequencyCurve.Evaluate (timer);

		leftScreen = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 10)).x;
		rightScreen = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 10)).x;
		topScreen = Camera.main.ScreenToWorldPoint (new Vector3(0, Camera.main.pixelHeight + 100, 10)).y;

		ModifiedTimeOfGame = TimeOfGame - TimeAfterComputing;
		ModifierTime = 100 - (1 - (TimeAfterComputing / TimeOfGame)) * 100;
		modifiedTOG = ModifiedTimeOfGame;

		slider = GameObject.Find ("TimeSlider").GetComponent<Slider> ();

		Coins = PlayerPrefs.GetInt ("Coins", 0);
	}
	[HideInInspector]
	public float modifiedTOG;

	float leftScreen;
	float rightScreen;
	float topScreen;

	void Update() {
		if (GameOver) {
			if (modifiedTOG <= 0.0f) {
				GameObject.Find ("LastPlanet").GetComponent<Animator> ().SetBool ("Enter", true);
				GameObject.Find ("BlackScreen").GetComponent<Animator> ().SetTrigger ("FadeIn");
			}

			return;
		}

		slider.value = timer / ModifiedTimeOfGame;

		startTimer -= Time.deltaTime;
		if (startTimer <= 0) {
			GamePause = false;
			startTimer = float.MaxValue;
			leftScreen = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 10)).x;
			rightScreen = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 10)).x;
			topScreen = Camera.main.ScreenToWorldPoint (new Vector3(0, Camera.main.pixelHeight + 100, 10)).y;
			ModifiedTimeOfGame = TimeOfGame - TimeAfterComputing;
			ModifierTime = 100 - (1 - (TimeAfterComputing / TimeOfGame)) * 100;
			modifiedTOG = ModifiedTimeOfGame;
		}

		if (GamePause) {
			return;
		}

		asteroidTimer += Time.deltaTime;
		bonusTimer += Time.deltaTime;
		timer += Time.deltaTime;
		modifiedTOG -= Time.deltaTime;

		if (modifiedTOG <= 0.0) {
			GameOver = true;
			return;
		}

		if (asteroidTimer >= asteroidToWait) {
			asteroidTimer = 0.0f;
			GameObject obj = AsteroidsPool.AskObject ();
			obj.transform.position = new Vector3 (Random.Range(leftScreen, rightScreen), topScreen, 1.91f);
			obj.GetComponent<AsteroidController> ().Speed = Random.Range(AsteroidsSpeedMin.Evaluate(timer), AsteroidsSpeedMax.Evaluate(timer));
			asteroidToWait = AsteroidsFrequencyCurve.Evaluate (timer);

		}

		if (bonusTimer >= bonusToWait) {
			bonusTimer = 0.0f;
			GameObject obj = BonusesPool.AskObject ();
			obj.transform.position = new Vector3 (Random.Range(leftScreen, rightScreen), topScreen, 1.91f);
			obj.GetComponent<ABonus> ().Speed = Random.Range(BonusSpeedMin.Evaluate(timer), BonusSpeedMax.Evaluate(timer));
			bonusToWait = BonusFrequencyCurve.Evaluate (timer);

		}
	}




	[HideInInspector]
	public Pool AsteroidsPool;
	[HideInInspector]
	public Pool BonusesPool;

	private float asteroidToWait = 0.0f;
	private float asteroidTimer = 0.0f;
	private float bonusToWait = 0.0f;
	private float bonusTimer = 0.0f;

	private float timer = 0.0f;
	private float startTimer = 3.0f;

	[HideInInspector]
	public bool GameOver = false;
	[HideInInspector]
	public bool GamePause = true;
}
