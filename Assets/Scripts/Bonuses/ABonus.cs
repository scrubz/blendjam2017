﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ABonus : MonoBehaviour {

	public float Speed = 1;

	// Use this for initialization
	void Start () {
		
	}

	public void Init(){
		// Change possible sprite here
	}

	// Update is called once per frame
	void Update () {
		if (GameManager.Instance.GameOver) {
			return;
		}
		if (GameManager.Instance.GamePause) {
			return;
		}

		if (transform.position.y < -12) {
			GameManager.Instance.BonusesPool.ReleaseObject (gameObject);
		}
	}

	void FixedUpdate() {
		if (GameManager.Instance.GameOver) {
			return;
		}
		if (GameManager.Instance.GamePause) {
			return;
		}

		transform.position += Vector3.down * Speed * Time.fixedDeltaTime;
	}

	public void GetBonus(){
		++GameManager.Instance.Coins;
		PlayerPrefs.SetInt ("Coins", GameManager.Instance.Coins);
	}
}
