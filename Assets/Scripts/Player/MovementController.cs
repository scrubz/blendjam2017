﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MovementController : MonoBehaviour {

	public float Speed = 1;
	public float AfterShotWait = 1;
	public int Resistance = 3;

	private char[] equips;
	private char[] upgrades;

	private int ORES;

	private bool canShoot = false;

	void AddBonuses(Equipment e) {
		Speed += e.GetAgility;
		Resistance += e.GetLife;
		GameManager.Instance.TimeAfterComputing += e.GetSpeedToFinish;
		if (e.CanShoot == true) {
			canShoot = true;
			AfterShotWait = e.Cooldown;
		}
	}

	// Use this for initialization
	void Start () {
		afterShotWaitTimer = 0;
		EquipedItems ei = GetComponent<EquipedItems> ();

		AddBonuses (ei.equipedArmsE);
		AddBonuses (ei.equipedTorsoE);
		AddBonuses (ei.equipedHeadE);
		ORES = Resistance;
		UpdateCoins ();
		fxmanager = GameObject.Find ("FxManager").GetComponent<FXManager>();
	}

	void Update() {
		if (GameManager.Instance.GameOver) {
			return;
		}
		if (GameManager.Instance.GamePause) {
			return;
		}


		afterShotWaitTimer -= Time.deltaTime;
	}

	float tEnd = 3.0f;

	// Update is called once per frame
	void FixedUpdate () {
		if (GameManager.Instance.GameOver) {
			if (GameManager.Instance.modifiedTOG <= 0.0f) {
				showGameOverUI (true, true);

				tEnd -= Time.fixedDeltaTime;
				if (tEnd <= 0.0f) {
					transform.position += Vector3.up * Speed * 2.5f * Time.fixedDeltaTime;
				}
			}
			return;
		}
		if (GameManager.Instance.GamePause) {
			return;
		}


		Vector3 mousePosition = Input.mousePosition;
		bool moveAllowed = true;

		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay(new Vector3(mousePosition.x, mousePosition.y, 0));
			Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
			RaycastHit hitInfo;
			if (Physics.Raycast (ray, out hitInfo)) {
				if (hitInfo.collider.tag == "Destroyable" && canShoot) {
					fxmanager.PutImpactFxAt (hitInfo.collider.transform.position);
					moveAllowed = false;
					GameManager.Instance.AsteroidsPool.ReleaseObject (hitInfo.collider.gameObject);
					afterShotWaitTimer = AfterShotWait;
				}
			else if (hitInfo.collider.tag == "Bonus") {
					fxmanager.PutBoulonFxAt (hitInfo.collider.transform.position);
				hitInfo.collider.GetComponent<ABonus> ().GetBonus ();
				GameManager.Instance.AsteroidsPool.ReleaseObject (hitInfo.collider.gameObject);
					UpdateCoins ();
				}
			}
		}

		if (afterShotWaitTimer <= 0.0f && Input.GetMouseButton (0) && moveAllowed) {
			int w = Screen.width;

			Vector3 leftScreen = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Vector3.Distance(Camera.main.transform.position, transform.position)));
			Vector3 rightScreen = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, Vector3.Distance(Camera.main.transform.position, transform.position)));

			if (mousePosition.x > w / 2) {
				transform.position += transform.right * Speed * Time.fixedDeltaTime;
				if (transform.position.x > rightScreen.x) {
					transform.position = new Vector3(rightScreen.x, transform.position.y, transform.position.z);
				}
			} else if (mousePosition.x < w / 2) {
				transform.position -= transform.right * Speed * Time.fixedDeltaTime;
				if (transform.position.x < leftScreen.x) {
					transform.position = new Vector3(leftScreen.x, transform.position.y, transform.position.z);
				}
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		if (GameManager.Instance.GameOver) {
			return;
		}
		if (other.tag == "Destroyable") {
			GameManager.Instance.AsteroidsPool.ReleaseObject (other.gameObject);
			--Resistance;
			GameObject.Find ("BottomPanel").GetComponent<Image> ().fillAmount = (float)((float)Resistance / (float)ORES);
			fxmanager.PutImpactFxAt (transform.position);
			if (Resistance <= 0) {
				GameManager.Instance.GameOver = true;
				showGameOverUI (true, false);
			}
		} else if (other.tag == "Bonus") {
			other.GetComponent<ABonus> ().GetBonus ();
			UpdateCoins ();
			GameManager.Instance.BonusesPool.ReleaseObject (other.gameObject);
			fxmanager.PutBoulonFxAt (transform.position);
		}
	}

	string[] levels = {
		"0",
		"1",
		"2",
	};

	public void showGameOverUI(bool b1, bool b2) {
		GameObject o = GameObject.Find ("UI").transform.GetChild(5).gameObject	;
		o.SetActive (true);
		o.transform.Find ("Text").GetComponent<Text>().text = b2 ? "Good Job !" : "Game Over";
		o.transform.Find ("RetryButton").gameObject.SetActive (b1);
		o.transform.Find ("NextButton").gameObject.SetActive (b2);
	}

	public void Retry() {
		SceneManager.LoadScene(levels[PlayerPrefs.GetInt("CurrentLevel")]);
	}

	public void Next() {
		PlayerPrefs.SetInt ("CurrentLevel", PlayerPrefs.GetInt ("CurrentLevel") + 1);
		SceneManager.LoadScene("RobotCreation");
	}

	void UpdateCoins() {
		GameObject.Find ("CoinText").GetComponent<TMPro.TextMeshProUGUI> ().text = GameManager.Instance.Coins + "";
	}

	private float afterShotWaitTimer = 0.0f;
	private FXManager fxmanager;
}
