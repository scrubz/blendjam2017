﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour {

	public GameObject Boulon_FX;
	public GameObject Impact_FX;

	public void PutBoulonFxAt(Vector3 pos) {
		GameObject go = GameObject.Instantiate (Boulon_FX, pos, Quaternion.identity);
		go.SetActive (true);
	}

	public void PutImpactFxAt(Vector3 pos) {
		GameObject go = GameObject.Instantiate (Impact_FX, pos, Quaternion.identity);
		go.SetActive (true);
	}
}
