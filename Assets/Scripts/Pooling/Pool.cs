﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour {

	public int NumberOfObjects = 20;
	public GameObject ObjectPrefab = null;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < NumberOfObjects; ++i) {
			GameObject obj = GameObject.Instantiate (ObjectPrefab);
			obj.SetActive (false);
			obj.transform.parent = transform;
			m_objects.Add (obj);
		}
	}

	public GameObject AskObject() {
		if (m_objects.Count > 0) {
			GameObject obj = m_objects [0];
			obj.SetActive (true);
			obj.transform.parent = null;
			m_objects.RemoveAt (0);
			return obj;
		}
		return null;
	}

	public void ReleaseObject(GameObject obj) {
		if (m_objects.Contains (obj) == false) {
			obj.SetActive (false);
			obj.transform.parent = transform;
			m_objects.Add (obj);
		}
	}

	private List<GameObject> m_objects = new List<GameObject>();
}
