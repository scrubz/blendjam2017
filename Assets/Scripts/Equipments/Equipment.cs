﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class Equipment {

	public enum gearType
	{
		Head,
		Body,
		Arms,
	}

	public string Name;
	public List<Values> Equipments;
	public int EquipedUpgrade = 0;
	public int Equiped = 0;

	//[XmlAttribute("Bought")]
	public int Bought = 0;
	//[XmlAttribute("BoughtUpgrade")]
	public int BoughtUpgrade = 0;

	[System.Serializable]
	public class Values {
		public int Agility = 0;
		public int SpeedToFinish = 0;
		public int Life = 0;
		public bool CanShoot = false;
		public float Cooldown = 1.0f;
		public int Price = 0;
		public int Niveau = 0;
		public string Name = "Default";
		public int PrefabId = 0;
		[XmlIgnore]
		public GameObject Model;
		public gearType GearType = gearType.Head;
	}

		public int GetAgility {
		get { return Equipments[EquipedUpgrade].Agility;}
		}

		public int GetSpeedToFinish {
		get { return Equipments[EquipedUpgrade].SpeedToFinish * 2; }
		}

		public int GetLife {
		get { return Equipments[EquipedUpgrade].Life; }
		}

	public bool CanShoot {
		get { return Equipments [equipment].CanShoot; }
	}

	public float Cooldown {
		get { return Equipments [equipment].Cooldown; }
	}
		
	void Start(){
		equipment = 0;
	}

	int equipment = 0;
}
